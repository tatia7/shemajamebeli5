package com.example.shemajamebeli5

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val URL = "https://run.mocky.io/"

    fun retrofitService(): RetrofitRepository{

        return Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).
        build().create(RetrofitRepository::class.java)
    }
}