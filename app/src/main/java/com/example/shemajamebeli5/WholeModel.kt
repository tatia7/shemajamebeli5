package com.example.shemajamebeli5

import android.media.Image

data class WholeModel(val field_id: String,
    val hint: String,
    val field_type:String,
    val keyboard: String,
    val required : Boolean,
    val is_active: Boolean,
    val icon : String)