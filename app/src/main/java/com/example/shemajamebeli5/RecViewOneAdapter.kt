package com.example.shemajamebeli5

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebeli5.databinding.RecviewoneBinding

class RecViewOneAdapter : RecyclerView.Adapter<RecViewOneAdapter.CardViewHolder>() {
    val JsonList : MutableList<MutableList<WholeModel>> = mutableListOf()
    private val viewPool = RecyclerView.RecycledViewPool()
    inner class CardViewHolder(private val binding: RecviewoneBinding): RecyclerView.ViewHolder(binding.root){
        val recyclerView: RecyclerView = binding.childRecView
    }

    fun addData(JsonList : MutableList<MutableList<WholeModel>>){
        this.JsonList.clear()
        this.JsonList.addAll(JsonList)
        notifyDataSetChanged()
        d("MyMessage", JsonList.toString())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val itemView = RecviewoneBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        val holder = CardViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val childLayout = LinearLayoutManager(holder.recyclerView.context)
        childLayout.initialPrefetchItemCount = 4
        holder.recyclerView.apply {
            layoutManager = childLayout
            adapter = RecViewTwoadapter(JsonList[position])
            setRecycledViewPool(viewPool)
        }
    }

    override fun getItemCount() = JsonList.size
}