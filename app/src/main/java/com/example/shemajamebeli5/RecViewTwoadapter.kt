package com.example.shemajamebeli5

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebeli5.databinding.RecviewtwoBinding

class RecViewTwoadapter(private val childList : List<WholeModel>) :
RecyclerView.Adapter<RecViewTwoadapter.childView> (){
    inner class childView(private val binding: RecviewtwoBinding) :
            RecyclerView.ViewHolder(binding.root){
                fun bind (){
                    val child = childList[adapterPosition]

                    val childText = binding.edittext
                    childText.hint = child.hint
                    childText.id = child.field_id.toInt()
                }
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): childView {
        val itemView = RecviewtwoBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val holder = childView(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: childView, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = childList.size
}