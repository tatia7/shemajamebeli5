package com.example.shemajamebeli5

import android.util.Log.d
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class jsonViewModel :  ViewModel() {
    private val jsonLiveData = MutableLiveData<MutableList<MutableList<WholeModel>>>().apply {
        MutableLiveData<MutableList<MutableList<WholeModel>>>()
    }

    val _jsonLiveData = jsonLiveData

    fun init(){
        viewModelScope.launch{
            withContext(Dispatchers.IO){
                getJson()
            }
        }
    }
    private suspend fun getJson(){
        val result = RetrofitService.retrofitService().getItems()
        d("MySecondMessage", result.toString())
        jsonLiveData.postValue(result.body() as MutableList<MutableList<WholeModel>>)
    }
}